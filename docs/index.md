# Python 환경 설정: 설치 가이드 <sup>[1](#footnote_1)</sup>

> <font size="3">Python을 설치하고 IDE를 설정하는 방법을 알아본다.</font>

## 목차

1. [소개](./setting-up-python-environment.md#소개)
1. [Python 설치하기](./setting-up-python-environment.md#python-설치하기)
1. [IDE 선택하기](./setting-up-python-environment.md#ide-선택하기)
1. [IDE 구성하기](./setting-up-python-environment.md#ide-구성하기)
1. [Python 스크립트 실행하기](./setting-up-python-environment.md#python-스크립트-실행하기)
1. [마치며](./setting-up-python-environment.md#마치며)

<a name="footnote_1">1</a>: [Python Tutorial 2 — Setting Up Python Environment: Installation Guide](https://medium.com/gitconnected/python-tutorial-2-setting-up-python-environment-installation-guide-d6a9dcadd2bc?sk=4c893365037e729a1e98020c80895055)를 편역한 것이다.